import React from 'react';
import Main from './Main';
import Header from './Header';
import { MediaPlayer } from './MediaPlayer';
import './style.css';

function App() {
  return (
    <div id='wrapperPlayer'>
      <MediaPlayer>
        <Main />
        <Header />
      </MediaPlayer>
    </div>
  );
}
export default App;
