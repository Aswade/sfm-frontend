import React, { useContext } from 'react';
import { mediaPlayerContext } from './MediaPlayer';
import './style.css';

function WrapperControls() {
  const {
    isPlaying,
    customAudioVolume,
    startPauseMusic,
    changeVolume,
    volumeIcon,
  } = useContext(mediaPlayerContext);

  return (
    <div id='wrapperControls'>
      <div id='volume'>
        <div id='volIcon' className='material-icons'>
          {volumeIcon}
        </div>
        <input
          className='range'
          type='range'
          id='volMeter'
          min='0'
          max='100'
          ref={customAudioVolume}
          onChange={changeVolume}
        />
      </div>
      <div id='queueIcon' className='material-icons'>
        queue_music
      </div>
      <div id='mediaplayer'>
        <div id='shuffle' className='material-icons'>
          shuffle
        </div>
        <div id='previous' className='material-icons'>
          skip_previous
        </div>
        <div
          id='togglePause'
          disabled
          className='material-icons'
          onClick={() => {
            startPauseMusic();
          }}
        >
          {isPlaying ? 'play_arrow' : 'pause'}
        </div>
        <div id='next' className='material-icons'>
          skip_next
        </div>
        <div id='repeat' className='material-icons'>
          repeat
        </div>
      </div>
      <div id='crrntInfo'></div>
    </div>
  );
}

export default WrapperControls;
