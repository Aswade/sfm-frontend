import React, { useState, useEffect, useContext } from 'react';
import Music from './Music';
import './style.css';
import { mediaPlayerContext } from './MediaPlayer';

function Main() {
  const {
    audioTag,
    fetchMusicList,
    fetchSongBufferAsB64,
    updateCustomAudioTimelineValue,
    musicList,
    audioSrc,
  } = useContext(mediaPlayerContext);

  const [isSearching, setIsSearching] = useState(false);

  useEffect(() => {
    fetchMusicList();
  }, []);
  return (
    <main>
      <div id='searchW'>
        <input type='text' id='search' placeholder='Search...' />
        <span id='searchBtn' className='material-icons search'>
          search
        </span>
      </div>
      <div id='info'>
        {musicList.map((music) => {
          return (
            <Music
              key={music._id}
              {...music}
              fetchSongBufferAsB64={fetchSongBufferAsB64}
            />
          );
        })}
      </div>
      <audio
        src={audioSrc}
        ref={audioTag}
        onTimeUpdate={updateCustomAudioTimelineValue}
        controls
      ></audio>
    </main>
  );
}

export default Main;
