import React, { useState } from 'react';

const mediaPlayerContext = React.createContext();

const audioTag = React.createRef();
const customAudioVolume = React.createRef();
const customAudioTimeline = React.createRef();

const MediaPlayer = ({ children }) => {
  const [isPlaying, setIsPlaying] = useState(true);
  const [audioCurrentTime, setAudioCurrentTime] = useState('0:00');
  const [musicList, setMusicList] = useState([]);
  const [audioSrc, setAudioSrc] = useState();
  const fetchMusicList = async () => {
    let response = await fetch('', {
      method: 'POST',
    });
    let responseJSONRaw = await response.json();
    let parsedResponseJSON = JSON.parse(responseJSONRaw);
    let responseData = parsedResponseJSON.response;
    setMusicList(responseData);
  };
  const fetchSongBufferAsB64 = async (musicSrc) => {
    let response = await fetch('', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ musicSrc: musicSrc }),
    });
    let bufferB64 = await response.text();
    const fetchB64 = await fetch(`data:audio/mp3;base64,${bufferB64}`);
    const blob = await fetchB64.blob();
    const url = URL.createObjectURL(blob);
    setAudioSrc(url);
  };
  const updateCustomAudioTimelineValue = () => {
    customAudioTimeline.current.max = audioTag.current.duration;
    let crrntTime = audioTag.current.currentTime;
    if (crrntTime > 0) {
      let m = Math.floor(crrntTime / 60);
      let s = Math.round(crrntTime - m * 60);

      if (s < 10) {
        s = '0' + s;
      }
      setAudioCurrentTime(`${m}:${s}`);
      customAudioTimeline.current.value = crrntTime;
    }
  };
  const changePlayTime = () => {
    let setVal = customAudioTimeline.current.value;
    audioTag.current.currentTime = setVal;
  };
  const [volumeIcon, setvolumeIcon] = useState('volume_down');
  const validBlobUrlRegExp =
    /^(blob:http[s]?:\/{2}[\w:.]+\/{1}[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12})$/;

  const startPauseMusic = () => {
    if (isPlaying && audioTag.current.src.match(validBlobUrlRegExp)) {
      setIsPlaying(!isPlaying);
      audioTag.current.play();
    }
    if (!isPlaying) {
      setIsPlaying(!isPlaying);
      audioTag.current.pause();
    }
  };
  const changeVolume = () => {
    let volumeVal = customAudioVolume.current.value;

    if (volumeVal <= 10) {
      setvolumeIcon('volume_mute');
    } else if (volumeVal >= 10 && volumeVal <= 60) {
      setvolumeIcon('volume_down');
    } else {
      setvolumeIcon('volume_up');
    }
    audioTag.current.volume = volumeVal / 100;
  };
  return (
    <mediaPlayerContext.Provider
      value={{
        isPlaying,
        setIsPlaying,
        audioTag,
        customAudioVolume,
        customAudioTimeline,
        audioCurrentTime,
        setAudioCurrentTime,
        fetchMusicList,
        fetchSongBufferAsB64,
        updateCustomAudioTimelineValue,
        musicList,
        audioSrc,
        changePlayTime,
        startPauseMusic,
        changeVolume,
        volumeIcon,
      }}
    >
      {children}
    </mediaPlayerContext.Provider>
  );
};

export { MediaPlayer, mediaPlayerContext };
