import React, { useContext } from 'react';
import WrapperControls from './WrapperControls';
import { mediaPlayerContext } from './MediaPlayer';
import './style.css';

function Header() {
  const { customAudioTimeline, audioCurrentTime, changePlayTime } =
    useContext(mediaPlayerContext);

  return (
    <header>
      <input
        className='range'
        type='range'
        id='timeline'
        min='0'
        max='100'
        onInput={changePlayTime}
        ref={customAudioTimeline}
        defaultValue={0}
      />
      <div id='timer'>{audioCurrentTime}</div>
      <WrapperControls />
    </header>
  );
}

export default Header;
