import React from 'react';
import ph from './ph.png';
import './style.css';

function Music(props) {
  const { title, author, filePath, fetchSongBufferAsB64 } = props;
  return (
    <div
      id='music'
      onClick={() => {
        fetchSongBufferAsB64(filePath);
      }}
    >
      <img id='ph' src={ph} alt='' />
      <span>{title}</span>
      <span>{author}</span>
    </div>
  );
}

export default Music;
